package nats

import (
	"fmt"

	"github.com/nats-io/nats.go"
)

type conn struct {
	nc *nats.Conn
}

func newConnection(connectionString string) (*conn, error) {
	connectString := connectionString
	if connectString == "" {
		connectString = nats.DefaultURL
	}
	// Connect to NATS
	nc, err := nats.Connect(connectString)
	if err != nil {
		return nil, fmt.Errorf("could not connect to nats at %q: %w", connectString, err)
	}
	return &conn{
		nc: nc,
	}, nil
}
