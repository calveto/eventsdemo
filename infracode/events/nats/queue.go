package nats

import (
	"fmt"
	"log"

	"github.com/nats-io/nats.go"
)

type queue struct {
	c *conn
}

func NewQueue(connectionString string) (*queue, error) {
	conn, err := newConnection(connectionString)
	if err != nil {
		return nil, err
	}
	return &queue{c: conn}, nil
}

func (q *queue) Consume(subject, consumerName string) (<-chan Event, error) {
	messages := make(chan Event)
	if _, err := q.c.nc.QueueSubscribe(subject, consumerName, func(msg *nats.Msg) {
		messages <- Event{
			RoutingKey: msg.Subject,
			Body:       msg.Data,
		}
	}); err != nil {
		return nil, fmt.Errorf("could not subscribe to %q. %w", subject, err)
	}
	return messages, nil
}

func (q *queue) Publish(subject string, msg []byte) error {
	log.Println("sending to", subject, ":", string(msg))
	err := q.c.nc.Publish(subject, msg)
	if err != nil {
		return fmt.Errorf("could not publish message %s to %s: %w", string(msg), subject, err)
	}
	return nil
}

func (q *queue) Live() error {
	if q.c.nc.IsConnected() {
		return nil
	}
	return fmt.Errorf("not connected to nats")
}

func (q *queue) Close() {
	if q.c.nc != nil {
		q.c.nc.Close()
	}
}
