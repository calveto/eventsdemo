package nats

type Event struct {
	RoutingKey string
	Body       []byte
}
