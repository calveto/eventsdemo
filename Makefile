SERVICE_BINARY=service
VALIDATOR_BINARY=validator
GRANTER_BINARY=granter

build:
	go build -o ${SERVICE_BINARY} cmd/service/main.go
	go build -o ${VALIDATOR_BINARY} cmd/validator/main.go
	go build -o ${GRANTER_BINARY} cmd/granter/main.go

run:
	go build -o ${SERVICE_BINARY} cmd/service/main.go
	go build -o ${VALIDATOR_BINARY} cmd/validator/main.go
	go build -o ${GRANTER_BINARY} cmd/granter/main.go
	./${SERVICE_BINARY} &
	./${VALIDATOR_BINARY} &
	./${GRANTER_BINARY} &

clean:
	go clean
	rm ${SERVICE_BINARY}
	rm ${VALIDATOR_BINARY}
	rm ${GRANTER_BINARY}
