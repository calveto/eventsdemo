# Developer Day (12/8/2022) Events Demo - NOT FOR PRODUCTION USE (See [starter repo generator](https://gitlab.ncci.com/sat/architecture/starter-repos/starter-repo-generator/-/blob/main/README.md))

### Requierements

1. Go 1.19.*
2. Have an instance of nats running
3. Have VS Code installed with the REST Client extension (id: humao.rest-client) installed

### How to run

1. Create `.env` file with the following:
```bash
NATS_URL=nats://natsddvl.ncci.com:4222/
```
2. Run the access granter and the request validator
```bash
$ go run cmd/granter/main.go
$ go run cmd/validator/main.go
```
3. Run the service `$ go run cmd/service/main.go`
4. Make a request to the service to create a product request (Use the requests.http file)
```
POST {{url}}/productrequest
Content-Type: application/json

{
    "contactId": 1,
    "productId": 1
}
```
