package main

import (
	"encoding/json"
	"eventsdemo/infracode/errors"
	"eventsdemo/infracode/events/nats"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

var (
	consumerName = "request-granter"
)

type productRequestModel struct {
	ContactID int `json:"contactId"`
	ProductID int `json:"productId"`
}

func main() {
	godotenv.Load()
	natsUrl := os.Getenv("NATS_URL")
	q, err := nats.NewQueue(natsUrl)
	errors.FailOnErr(err)
	err = q.Live()
	errors.FailOnErr(err)
	messages, err := q.Consume("requests.*.validated", consumerName)
	errors.FailOnErr(err)
	for msg := range messages {
		log.Println("Handling", msg.RoutingKey, string(msg.Body))
		subjectParts := strings.Split(msg.RoutingKey, ".")
		if len(subjectParts) < 3 {
			log.Println("cannot handle request with less than 3 parts in the subject")
			continue
		}
		id := subjectParts[1]
		var pr productRequestModel
		err := json.Unmarshal(msg.Body, &pr)
		if err != nil {
			if err := sendError(q, id, err); err != nil {
				log.Println("could not send error", err.Error())
			}
			continue
		}
		// Handle product granting rules
		if pr.ProductID > 5 {
			if err := sendError(q, id, fmt.Errorf("cannot grant access to unknown product %d", pr.ProductID)); err != nil {
				log.Println("could not send error", err.Error())
			}
			continue
		}
		requestSuccededSubject := fmt.Sprintf("requests.%s.succeded", id)
		if err := q.Publish(requestSuccededSubject, []byte("done")); err != nil {
			log.Println("error publishing success for", id)
			continue
		}
	}
}

type queue interface {
	Publish(sub string, body []byte) error
}

func sendError(q queue, id string, err error) error {
	errorRequestSubject := fmt.Sprintf("requests.%s.error", id)
	if err := q.Publish(errorRequestSubject, []byte(err.Error())); err != nil {
		return fmt.Errorf("error publishing error for %s", id)
	}
	return nil
}
