package main

import (
	"encoding/json"
	"eventsdemo/infracode/errors"
	"eventsdemo/infracode/events/nats"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/joho/godotenv"
)

var (
	consumerName = "product-request-service"
)

type productRequestModel struct {
	ContactID int `json:"contactId"`
	ProductID int `json:"productId"`
}

func main() {
	godotenv.Load()
	natsUrl := os.Getenv("NATS_URL")
	q, err := nats.NewQueue(natsUrl)
	errors.FailOnErr(err)
	defer q.Close()
	err = q.Live()
	errors.FailOnErr(err)
	http.HandleFunc("/", createProductRequestRoute(q))
	log.Fatal(http.ListenAndServe(":40100", nil))
}

func createProductRequestRoute(q queue) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "could not read product request; %s", err.Error())
			return
		}
		var pr productRequestModel
		if err := json.Unmarshal(b, &pr); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "could not parse product request; %s", err.Error())
			return
		}
		if err := createProductRequest(q, pr); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "could not create product request; %s", err.Error())
			return
		}
		fmt.Fprintf(w, "product request created")
	}
}

type queue interface {
	Publish(sub string, body []byte) error
	Consume(subject, consumerName string) (<-chan nats.Event, error)
}

type productRequestResponse struct {
	msg string
	err error
}

func createProductRequest(p queue, pr productRequestModel) error {
	jsonProductRequest, err := json.Marshal(pr)
	if err != nil {
		return fmt.Errorf("could not marshal product request; %w", err)
	}
	rawUUID := uuid.New()
	newRequestSubject := fmt.Sprintf("requests.%s.new", rawUUID.String())
	successRequestSubject := fmt.Sprintf("requests.%s.succeded", rawUUID.String())
	errorRequestSubject := fmt.Sprintf("requests.%s.error", rawUUID.String())
	responseDoneChannel := make(chan productRequestResponse)
	successMessagesChannel, err := p.Consume(successRequestSubject, consumerName)
	if err != nil {
		return fmt.Errorf("could not set up success channel; %w", err)
	}
	errorMessagesChannel, err := p.Consume(errorRequestSubject, consumerName)
	if err != nil {
		return fmt.Errorf("could not set up error channel; %w", err)
	}
	go waitOnResponse(4, responseDoneChannel, successMessagesChannel, errorMessagesChannel)
	err = p.Publish(newRequestSubject, jsonProductRequest)
	if err != nil {
		return fmt.Errorf("could not publish product request; %w", err)
	}
	d := <-responseDoneChannel
	return d.err
}

func waitOnResponse(waitTimeSeconds int, responseDoneChannel chan<- productRequestResponse, successMessagesChannel, errorMessagesChannel <-chan nats.Event) {
	timeOutChannel := time.After(time.Duration(time.Duration(waitTimeSeconds) * time.Second))
	defer close(responseDoneChannel)
	select {
	case successEvent := <-successMessagesChannel:
		responseDoneChannel <- productRequestResponse{
			msg: string(successEvent.Body),
		}
	case errEvent := <-errorMessagesChannel:
		responseDoneChannel <- productRequestResponse{
			err: fmt.Errorf(string(errEvent.Body)),
		}
	case <-timeOutChannel:
		responseDoneChannel <- productRequestResponse{
			err: fmt.Errorf("request didn't finish in time, timing out"),
		}
	}
}
